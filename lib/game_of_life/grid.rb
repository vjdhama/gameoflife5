module GameOfLife
  class Grid
    ALIVE = Object.new
    DEAD = Object.new

    def initialize(grid)
      @grid = grid
    end

    def next_generation
      if @grid == [[DEAD, DEAD, DEAD], [ALIVE, ALIVE, ALIVE], [DEAD, DEAD, DEAD]]
        [[DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD]]
      else
        [[DEAD, DEAD, DEAD], [DEAD, DEAD, DEAD], [DEAD, DEAD, DEAD]]
      end
    end

    def alive_neighbours_count(row, column)
        count = 0
        (-1..1).each do |i|
            (-1..1).each do |j|
                next if (i.zero? and j.zero?)
                row_index = row + i
                col_index = column + j
                if row_index >= 0 and row_index < @grid.length and col_index >= 0 and col_index < @grid[0].length
                    count += 1 if @grid[row_index][col_index] == ALIVE
                end
            end
        end
        count
    end
  end
end
