require 'spec_helper'

module GameOfLife
  describe Grid do
    context "Next generation" do 
      ALIVE = Grid::ALIVE
      DEAD = Grid::DEAD

      it "should work on standard oscillator input" do
        base_grid = [[DEAD, DEAD, DEAD], [ALIVE, ALIVE, ALIVE], [DEAD, DEAD, DEAD]]
        grid = Grid.new(base_grid)
        next_grid = [[DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD]]
        expect(grid.next_generation).to eq(next_grid)  
      end

      it "should give grid with all dead cells when input is the same" do
        base_grid = [[DEAD, DEAD, DEAD], [DEAD, DEAD, DEAD], [DEAD, DEAD, DEAD]]
        grid = Grid.new(base_grid)
        next_grid = [[DEAD, DEAD, DEAD], [DEAD, DEAD, DEAD], [DEAD, DEAD, DEAD]]
        expect(grid.next_generation).to eq(next_grid)  
      end
    end

    it "should return correct number of alive neighbours" do
      base_grid = [[DEAD, DEAD, DEAD], [ALIVE, ALIVE, ALIVE], [DEAD, DEAD, DEAD]]
      grid = Grid.new(base_grid)
      expect(grid.alive_neighbours_count(1, 1)).to eq(2)
    end
  end
end
